<?php
require_once('../includes/common.php');
$common_obj= new Common();

 $stud=$common_obj->getstudentsdet();
?>
<!DOCTYPE html>

<html lang="en">
<head>
	<meta charset="utf-8">
	<title>SimpleAdmin - Dashboard</title>
	
	<!-- Stylesheets -->
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet'>
	<link rel="stylesheet" href="CSS/stylenew.css">
	
	<!-- Optimize for mobile devices -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	
	<!-- jQuery & JS files -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="js/script.js"></script>  
    
</head>
<body>

	<!-- TOP BAR -->
	<?php include("../includes/topbar.php"); ?>
     <!-- end top-bar -->
	
	
	
	<!-- HEADER -->
	<div id="header-with-tabs">
		
		<div class="page-full-width cf">
	
			<ul id="tabs" class="fl">
				<li><a href="dashboard.html" class="active-tab dashboard-tab">Dashboard</a></li>
				
			</ul> <!-- end tabs -->
			
			<!-- Change this image to your own company's logo -->
			<!-- The logo will automatically be resized to 30px height. -->
		<a href="#" id="company-branding-small" class="fr">Infoark Education</a>
			
		</div> <!-- end full-width -->	

	</div> <!-- end header -->
	
	
	<?php $uname=$_SESSION["Username"];?>
<?php  $regid=$_SESSION["UserId"];    ?>
<?php  $utype=$_SESSION["Usertype"];    ?>
<?php $stu=$_GET["stuid"]; ?>

<?php
if(isset($_GET["opr"]) &&($_GET["opr"]=="app"))
{
	
	$updateid=$stu;
	
	 $values = array();	
		 
	$values['Status']  ='1';
		$table='stud';
	$condition="where StudId='". $updateid."'";
	 
	$common_obj->update( $values,$table,$condition);
	
	
	
	/*$qryupd="update stud set Status='1' where StudId=".$stu."";
 
	mysql_query($qryupd);*/
}

?>

	<!-- MAIN CONTENT -->
	<div id="content">
		
		<div class="page-full-width cf">

			<?php include("../includes/admin_sidebar.php"); ?>
			
			<div class="side-content fr">
			
				<div class="content-module">
				
					<div class="content-module-heading cf">
					
						<h3 class="fl">Students View</h3>
						<span class="fr expand-collapse-text">Click to collapse</span>
						<span class="fr expand-collapse-text initial-expand">Click to expand</span>
					
					</div> <!-- end content-module-heading -->
					
					
					<div class="content-module-main">
					
						<table>
						
							<thead>
						
								<tr>
									<th><input type="checkbox" id="table-select-all"></th>
									<th>Student Name</th>
									<th>Address</th>
									<th>Mobile No</th>
                                    <th>E-mail</th>
                                    <th>Class No</th>
                                    <th>Block Name</th>
                                    <th>Status</th>
									<th>Actions</th>
								</tr>
                                 <?php
          
		 /* $qrystud="select StudId,Name,Address,Mobile,Email,Image,classNo,BlockName,Status from stud      ";
		
	 
	      $resultstud=mysql_query($qrystud); */
		 while($stude=mysql_fetch_object($stud))
				  { ?>
							
							</thead>
	
							<tfoot>
							
								                               
							
							</tfoot>
							
							<tbody>
	
								<tr>
									<td><input type="checkbox"></td>
									<td><?php echo $stude->Name ?></td>
									<td><?php echo $stude->Address ?></td>
                                    <td><?php echo $stude->Mobile ?></td>
                                    <td><?php echo $stude->Email ?></td>
                                     <td><?php echo $stude->classNo ?></td>
                                     <td><?php echo $stude->BlockName ?></td>
                                     <td><?php if($rows["Status"]=='0') echo 'Approval Pending';   else  echo 'Approved'; ?> 
					</td>
									
									<td>
										<a href="viewstudents.php?stuid=<?php echo $stude->StudId ?>&opr=app" class="table-actions-button ic-table-edit"></a>
										
									</td>
								</tr>
                                
                                <!--<tr>
								
									<td colspan="5" class="table-footer">
									
										<label for="table-select-actions">With selected:</label>
	
										<select id="table-select-actions">
											<option value="option1">Delete</option>
											<option value="option2">Export</option>
											<option value="option3">Archive</option>
										</select>
										
										<a href="#" class="round button blue text-upper small-button">Apply to selected</a>	
	
									</td>-->
									
								</tr>

	
								 <?php } ?>
							
							</tbody>
							
						</table>
					
					</div> <!-- end content-module-main -->
				
				</div> <!-- end content-module --><!-- end content-module --></div> <!-- end side-content -->
		
		</div> <!-- end full-width -->
			
	</div> <!-- end content -->
	
	
	
	<!-- FOOTER -->
	<div id="footer">

		<p>&copy; Copyright 2012 <a href="#">BlueHosting, LLC</a>. All rights reserved.</p>
		<p><strong>SimpleAdmin</strong> theme by <a href="http://www.adipurdila.com">Adi Purdila</a></p>
	
	</div> <!-- end footer -->

</body>
</html>