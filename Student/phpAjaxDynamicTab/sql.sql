CREATE TABLE IF NOT EXISTS `tab_cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tab_cat`
--

INSERT INTO `tab_cat` (`id`, `cat_name`) VALUES
(1, 'ajax'),
(2, 'php'),
(3, 'htaccess');

-- --------------------------------------------------------

--
-- Table structure for table `tab_tutorial`
--

CREATE TABLE IF NOT EXISTS `tab_tutorial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `tutorial` varchar(500) NOT NULL,
  `link` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `tab_tutorial`
--

INSERT INTO `tab_tutorial` (`id`, `cat_id`, `tutorial`, `link`) VALUES
(1, 1, 'PHP Cookie Ajax Based Poll Script', 'http://www.91weblessons.com/php-cookie-ajax-based-poll-script/'),
(2, 1, 'Live Username Availability Check with Ajax and PHP', 'http://www.91weblessons.com/live-username-availability-check-with-ajax-and-php/'),
(3, 1, 'PHP Ajax Pagination Tutorial', 'http://www.91weblessons.com/php-ajax-pagination-tutorial/'),
(4, 1, 'PHP Cookie Ajax Based Rating Script', 'http://www.91weblessons.com/php-cookie-ajax-based-rating-script/'),
(5, 1, 'PHP Ajax Login Validation Tutorial', 'http://www.91weblessons.com/php-ajax-login-validation-tutorial/'),
(6, 1, 'PHP Cookie Ajax Based Like Dislike Script', 'http://www.91weblessons.com/php-cookie-ajax-based-like-dislike-script/'),
(7, 1, 'Upload Form using Ajax Jquery', 'http://www.91weblessons.com/upload-form-using-ajax-jquery/'),
(8, 2, 'PHP Cookie Ajax Based Poll Script', 'http://www.91weblessons.com/php-cookie-ajax-based-poll-script/'),
(9, 2, 'How to create custom module in drupal 7', 'http://www.91weblessons.com/how-to-create-custom-module-in-drupal-7/'),
(10, 2, 'Live Username Availability Check with Ajax and PHP', 'http://www.91weblessons.com/live-username-availability-check-with-ajax-and-php/'),
(11, 2, 'PHP Ajax Pagination Tutorial', 'http://www.91weblessons.com/php-ajax-pagination-tutorial/'),
(12, 2, 'PHP Cookie Ajax Based Rating Script', 'http://www.91weblessons.com/php-cookie-ajax-based-rating-script/'),
(13, 2, 'How to Create Facebook Application Using PHP and Retrieve User Information', 'http://www.91weblessons.com/how-to-create-facebook-application-using-php-and-retrieve-user-information/'),
(14, 2, 'PHP Ajax Login Validation Tutorial', 'http://www.91weblessons.com/php-ajax-login-validation-tutorial/'),
(15, 2, 'PHP MVC Framework Tutorial', 'http://www.91weblessons.com/php-mvc-framework-tutorial/'),
(16, 2, 'PHP Cookie Ajax Based Like Dislike Script', 'http://www.91weblessons.com/php-cookie-ajax-based-like-dislike-script/'),
(17, 2, 'cURL with PHP', 'http://www.91weblessons.com/curl-with-php/'),
(18, 2, 'MySql Prepared Statements with PHP', 'http://www.91weblessons.com/mysql-prepared-statements-with-php/'),
(19, 2, 'Basic PHP Validations with regular expression', 'http://www.91weblessons.com/basic-php-validations-with-regular-expression/'),
(20, 2, 'Upload Form using Ajax Jquery', 'http://www.91weblessons.com/upload-form-using-ajax-jquery/'),
(21, 2, 'Getting the Real IP Address of Website Visitors in PHP', 'http://www.91weblessons.com/getting-the-real-ip-address-of-website-visitors-in-php/'),
(22, 2, 'What is PHP and Why PHP is Popular', 'http://www.91weblessons.com/what-is-php/'),
(23, 3, 'Hide .php extension with url rewriting using .htaccess', 'http://www.91weblessons.com/hide-php-extension-with-url-rewriting-using-htaccess/'),
(24, 3, 'Custom Error Pages Using HTACCESS', 'http://www.91weblessons.com/custom-error-pages-using-htaccess/'),
(25, 3, 'SEO Friendly URL using HTACCESS', 'http://www.91weblessons.com/seo-friendly-url-using-htaccess/'),
(26, 3, 'What is HTACCESS', 'http://www.91weblessons.com/what-is-htaccess/');