-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 13, 2014 at 05:43 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ark_edu`
--

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE IF NOT EXISTS `answer` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `AnswerId` int(11) NOT NULL,
  `QuestionId` int(11) NOT NULL,
  `Answer` varchar(1000) NOT NULL,
  `UserId` int(11) NOT NULL,
  `dDate` date NOT NULL,
  `Status` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `answer`
--

INSERT INTO `answer` (`Id`, `AnswerId`, `QuestionId`, `Answer`, `UserId`, `dDate`, `Status`) VALUES
(7, 100, 1002, '<p><strong>jAVA CURIOSITIES</strong></p>\r\n<p>Hi guys,<br /><br />These are just some questions that worries me.<br /><br />1.) ArrayList&lt;String&gt; list1 = new List&lt;String&gt; /*WRONG*/<br />2.) List&lt;String&gt; list2 = new ArrayList&lt;String&gt;() /*CORRECT*/<br />3.) ArrayList&lt;String&gt; list3 = new ArrayList&lt;String&gt;() /*CORRECT*/<br />4.) List&lt;String&gt; list 4 = new List&lt;String&gt;()/*WRONG*/<br /><br />What could be the reasons why (1), and (4) are illegal but (2) and (3) are legal?</p>', 101, '2014-07-19', 1),
(13, 102, 1002, '<p>c++ provides four storage class modifiers :auto,register,static and extern</p>', 100, '2014-10-09', 1),
(14, 103, 1002, '<p>hgkljkgjjhhjgfgh</p>', 100, '2014-10-09', 1),
(15, 104, 1002, '<p>hgkljkgjjhhjgfgh</p>', 0, '2014-10-09', 1),
(12, 101, 1, '<p>An object is an identifiable entity with some characteristics and behaviour.It represents an entity that can store data and its associated functions</p>', 101, '2014-10-09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL,
  `Usertype` varchar(15) NOT NULL,
  `Status` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`Id`, `UserId`, `Username`, `password`, `Usertype`, `Status`) VALUES
(1, 10, 'Admin', 'admin1234', 'Admin', 1),
(2, 1000, 'ram45', '123456', 'Student', 1),
(3, 1001, 'meenu22', '123456mm', 'Student', 1),
(4, 1002, 'reshma45', '123456', 'Student', 1),
(25, 102, 'mad99', '1234', 'Teacher', 1),
(23, 101, 'ram78', '123456ph', 'Teacher', 1),
(22, 100, 've45', '123456', 'Teacher', 1);

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `QuestionId` int(11) NOT NULL,
  `Questions` varchar(80) NOT NULL,
  `UserId` int(11) NOT NULL,
  `Qdate` date NOT NULL,
  `Status` int(11) NOT NULL,
  `link` varchar(100) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`Id`, `QuestionId`, `Questions`, `UserId`, `Qdate`, `Status`, `link`) VALUES
(7, 1001, 'Ubuntu General Support\r\nChoose the most appropriat', 1000, '2014-07-03', 1, 'questionanswer_admin.php'),
(34, 1, '<p>What is the difference beteween an object and class</p>', 1000, '2014-10-09', 1, 'questionanswer_admin.php');

-- --------------------------------------------------------

--
-- Table structure for table `stud`
--

CREATE TABLE IF NOT EXISTS `stud` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `StudId` int(11) NOT NULL,
  `Name` varchar(25) NOT NULL,
  `Address` varchar(50) NOT NULL,
  `Mobile` int(11) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `Image` varchar(80) NOT NULL,
  `classNo` int(11) NOT NULL,
  `BlockName` varchar(20) NOT NULL,
  `Status` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `stud`
--

INSERT INTO `stud` (`Id`, `StudId`, `Name`, `Address`, `Mobile`, `Email`, `Image`, `classNo`, `BlockName`, `Status`) VALUES
(14, 1001, 'Meenu Mohan', 'Chandran villa,Kallada South,Kuyumathikadu,Kundr,K', 2147483647, 'meenu@gmail.com', '', 125, 'theja', 1),
(13, 1000, 'Ramu mohan', 'Ramesh Vilas,Bhepoore,Kozhikodu,Near Palayam Marke', 2147483647, 'rr@gmail.com', '5544409.jpg', 110, 'pamba', 1),
(15, 1002, 'Charles Tv', 'Jesus villa,Mananthavady,kozhikodu', 2147483647, 'reshma@gmail.com', 'download.jpg', 185, 'thena', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tab_cat`
--

CREATE TABLE IF NOT EXISTS `tab_cat` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(50) NOT NULL,
  `days` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tab_cat`
--

INSERT INTO `tab_cat` (`id`, `cat_name`, `days`) VALUES
(1, 'New Question', 14),
(2, 'Previous Question', 15);

-- --------------------------------------------------------

--
-- Table structure for table `teacher_registeration`
--

CREATE TABLE IF NOT EXISTS `teacher_registeration` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `TeacId` int(11) NOT NULL,
  `Name` varchar(15) NOT NULL,
  `Address` varchar(60) NOT NULL,
  `MobNo` int(11) NOT NULL,
  `Email` varchar(25) NOT NULL,
  `Photo` varchar(30) NOT NULL,
  `Jdate` date NOT NULL,
  `Institution` varchar(25) NOT NULL,
  `Designation` varchar(30) NOT NULL,
  `Status` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `teacher_registeration`
--

INSERT INTO `teacher_registeration` (`Id`, `TeacId`, `Name`, `Address`, `MobNo`, `Email`, `Photo`, `Jdate`, `Institution`, `Designation`, `Status`) VALUES
(2, 100, 'Venu', 'Venu dale\r\nkollam', 2147483647, 'rr@gmail.com', '5544409.jpg', '1990-02-12', 'S.N college', 'Teacher', 1),
(3, 101, 'Ramesh Kumar', 'Ramesh villa\r\nKadavanthara\r\nPanampallil nagar\r\nKochi-14', 2147483647, 'ram@gmail.com', '5544409.jpg', '1991-02-20', 'Fathima National College', 'Physics HOD', 1),
(12, 102, 'Madhavan Menon', 'madhavan vilas\r\nErnakulam', 2147483647, 'vv@fmail.com', 'images (9).jpg', '1991-02-12', 'Denis college', 'Physics HOD', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
