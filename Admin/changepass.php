<?php
require_once('../includes/common.php');
$common_obj= new Common();
?>

<!DOCTYPE html>

<html lang="en">
<head>
	<meta charset="utf-8">
	<title>SimpleAdmin - Dashboard</title>
	
	<!-- Stylesheets -->
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet'>
	<link rel="stylesheet" href="CSS/stylenew.css">
	
	<!-- Optimize for mobile devices -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	
	<!-- jQuery & JS files -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="js/script.js"></script>  
    
</head>
<body>

	<!-- TOP BAR -->
	<?php include("../includes/topbar.php"); ?>
     <!-- end top-bar -->
	
	
	
	<!-- HEADER -->
	<div id="header-with-tabs">
		
		<div class="page-full-width cf">
	
			<ul id="tabs" class="fl">
				<li><a href="dashboard.html" class="active-tab dashboard-tab">Dashboard</a></li>
				
			</ul> <!-- end tabs -->
			
			<!-- Change this image to your own company's logo -->
			<!-- The logo will automatically be resized to 30px height. -->
			<a href="#" id="company-branding-small" class="fr">Infoark Education</a>
			
		</div> <!-- end full-width -->	

	</div> <!-- end header -->
	
	
	
	<!-- MAIN CONTENT -->
	<div id="content">
		
		<div class="page-full-width cf">

			<?php include("../includes/admin_sidebar.php"); ?>
			
			<div class="side-content fr"><!-- end content-module -->
				
			  <div class="content-module">
				
					<div class="content-module-heading cf">
					
						<h3 class="fl">Chamge Password</h3>
						<span class="fr expand-collapse-text">Click to collapse</span>
						<span class="fr expand-collapse-text initial-expand">Click to expand</span>
					
					</div> <!-- end content-module-heading -->
					
					
					<div class="content-module-main cf">
				
						<div class="half-size-column fl">
                        <?php
                       if(isset($_POST["add"]))
{
	$updateid='Admin';
	 $values = array();	
		 
	$values['password']  =$common_obj->Tostring(addslashes($_POST["pass"]));
	$table='login';
	$condition="where Usertype='". $updateid."'";
	 
	$common_obj->update( $values,$table,$condition);
	
	header("location:logout.php");
}

?>
						
							<form action="#" method="post">
							
								<fieldset>
								
									<p>
										<label for="simple-input">Change Password</label>
									  <input type="password" id="simple-input" name="pass" class="round default-width-input" />
									</p>
                                    
                                    <p>
										<label for="simple-input">Repeat Password</label>
									  <input type="password" id="simple-input" name="passrep" class="round default-width-input" />
									</p>
									
									
	<input type="submit" value="Submit Button" name="add" class="round blue ic-right-arrow" />
									
	
									
									
								</fieldset>
							
							</form>
						
						</div> <!-- end half-size-column -->
						
						<div class="half-size-column fr"></div> 
						<!-- end half-size-column -->
				
					</div> <!-- end content-module-main -->
					
				</div> <!-- end content-module --></div> <!-- end side-content -->
		
		</div> <!-- end full-width -->
			
	</div> <!-- end content -->
	
	
	
	<!-- FOOTER -->
	<div id="footer">

		<p>&copy; Copyright 2012 <a href="#">BlueHosting, LLC</a>. All rights reserved.</p>
		<p><strong>SimpleAdmin</strong> theme by <a href="http://www.adipurdila.com">Adi Purdila</a></p>
	
	</div> <!-- end footer -->

</body>
</html>