<?php 
session_start(); ?>
<?php $uname=$_SESSION["Username"];?>
<?php  $regid=$_SESSION["UserId"];    ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="EN" lang="EN" dir="ltr">
<head profile="http://gmpg.org/xfn/11">
<title>Educational</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="imagetoolbar" content="no" />
<link rel="stylesheet" href="styles/layout.css" type="text/css" />
<script type="text/javascript" src="scripts/jquery-1.4.1.min.js"></script>
<script type="text/javascript" src="scripts/jquery.slidepanel.setup.js"></script>
<script type="text/javascript" src="scripts/jquery-ui-1.7.2.custom.min.js"></script>
<script type="text/javascript" src="scripts/jquery.tabs.setup.js"></script>
</head>
<body>
<div class="wrapper col0">
  <div id="topbar">
    <div id="slidepanel">
      <div class="topbox">
        <!--<h2>Login Area For Members</h2>
        <p>Teachers and students can login in this area.Only For Registered Members</p>
        <p class="readmore"><a href="opr/student_registration.php">Continue Reading &raquo;</a></p>
      </div>
      <div class="topbox">
        <h2>Teachers Login Here</h2>
        <form action="#" method="post">
          <fieldset>
            <legend>Teachers Login Form</legend>
            <label for="teachername">Username:
              <input type="text" name="teachername" id="teachername" value="" />
            </label>
            <label for="teacherpass">Password:
              <input type="password" name="teacherpass" id="teacherpass" value="" />
            </label>
            <label for="teacherremember">
              <input class="checkbox" type="checkbox" name="teacherremember" id="teacherremember" checked="checked" />
              Remember me</label>
            <p>
              <input type="submit" name="teacherlogin" id="teacherlogin" value="Login" />
              &nbsp;
              <input type="reset" name="teacherreset" id="teacherreset" value="Reset" />
            </p>
          </fieldset>
        </form>
      </div>
      <div class="topbox last">
        <h2>Pupils Login Here</h2>
        <form action="#" method="post">
          <fieldset>
            <legend>Pupils Login Form</legend>
            <label for="pupilname">Username:
              <input type="text" name="pupilname" id="pupilname" value="" />
            </label>
            <label for="pupilpass">Password:
              <input type="password" name="pupilpass" id="pupilpass" value="" />
            </label>
            <label for="pupilremember">
              <input class="checkbox" type="checkbox" name="pupilremember" id="pupilremember" checked="checked" />
              Remember me</label>
            <p>
              <input type="submit" name="pupillogin" id="pupillogin" value="Login" />
              &nbsp;
              <input type="reset" name="pupilreset" id="pupilreset" value="Reset" />
            </p>
          </fieldset>
        </form>-->
      </div>
      <br class="clear" />
    </div>
    <div id="loginpanel">
      <ul>
        <li class="left">Log In Here &raquo;</li>
        <li class="right" id="toggle"><a id="slideit" href="logout.php">Logout</a><a id="closeit" style="display: none;" href="#slidepanel">Close Panel</a></li>
      </ul>
    </div>
    <br class="clear" />
  </div>
</div>
<!-- ####################################################################################################### -->
<div class="wrapper col1">
  <div id="header">
    <div id="logo">
      <h1><a href="#">Infoark Education</a></h1>
      <p>Education from experts for Society</p>
    </div>
    <div class="fl_right">
      <ul>
        <li class="last"><a href="#">Search</a></li>
        <li><a href="http://infoark.in/contact.php">Online Support</a></li>
        <li><a href="#">School Board</a></li>
      </ul>
      <p>Tel:+91 9645272915 | Mail: infoarksoftwares@gmail.com</p>
    </div>
    <br class="clear" />
  </div>
</div>
<!-- ####################################################################################################### -->
<div class="wrapper col2">
  <div id="topnav">
    <ul>
      <li class="active"><a href="index.html">Home</a>
        <ul>
         <!-- <li><a href="#">Libero</a></li>
          <li><a href="#">Maecenas</a></li>
          <li><a href="#">Mauris</a></li>
          <li class="last"><a href="#">Suspendisse</a></li>-->
        </ul>
      </li>
     <!-- <li><a href="style-demo.html">Style Demo</a>
        <ul>
          <li><a href="#">Lorem ipsum dolor</a></li>
          <li><a href="#">Suspendisse in neque</a></li>
          <li class="last"><a href="#">Praesent et eros</a></li>
        </ul>
      </li>
      <li><a href="full-width.html">Full Width</a>
        <ul>
          <li><a href="#">Lorem ipsum dolor</a></li>
          <li><a href="#">Suspendisse in neque</a></li>
          <li class="last"><a href="#">Praesent et eros</a></li>
        </ul>
      </li>
      <li><a href="#">Our Services</a></li>
      <li class="last"><a href="#">Long Link Text</a></li>-->
    </ul>
  </div>
</div>
<!-- ####################################################################################################### -->
<div class="wrapper col3">
  <div id="featured_slide"></div>
</div>
<!-- ####################################################################################################### -->
<div class="wrapper col4">
  <div id="container">
    <div id="hpage">
      <ul>
        <li>
          <h2><a href="http://www.results.itschool.gov.in/" style="color:#FF0000;">Questions& Answers </a></h2>
          <div class="imgholder"><a href="Teacher/tabs.php"><img src="Icons/1404395359_Help.png" alt=""  style="width:200px;height:140px;"/></a></div>
          <p><br/></p>
        </li>
        <li>
          <h2>Profile</h2>
          <div class="imgholder"><a href="Teacher/profile.php" style="color:#FF0000;"><img src="Icons/1405006510_Manager.png" alt="" /></a></div>
          <p>&nbsp;</p>
          <p class="readmore">&nbsp;</p>
        </li>
        <li>
          <h2>Logout</h2>
          <div class="imgholder"><a href="logout.php"><img src="Icons/1405006731_Logout.png" alt="" /></a></div>
          <p>&nbsp;</p>
          <p class="readmore">&nbsp;</p>
        </li>
        
      </ul>
      <br class="clear" />
    </div>
  </div>
</div>
<!-- ####################################################################################################### -->
<div class="wrapper col5">
  <div id="footer">
    <div id="newsletter">
      <h2>Stay In The Know !</h2>
      <p>Please enter your email to join our mailing list</p>
      <form action="http://infoark.in/contact.php" method="post">
        <fieldset>
          <legend>News Letter</legend>
          <input type="text" value="Enter Email Here&hellip;"  onfocus="this.value=(this.value=='Enter Email Here&hellip;')? '' : this.value ;" />
          <input type="submit" name="news_go" id="news_go" value="GO" />
        </fieldset>
      </form>
      <p>To unsubscribe please <a href="http://infoark.in/contact.php">click here &raquo;</a></p>
    </div>
    <div class="footbox">
      <h2>Career Guidance</h2>
      <ul>
        <li><a href="#">Admissions</a></li>
        <li><a href="#">How To Learn be successfull</a></li>
        <li><a href="#">Private coaching</a></li>
        <li><a href="#">Other Activities</a></li>
        <li class="last"><a href="#">Arts And Crafts</a></li>
      </ul>
    </div>
    <div class="footbox">
      <h2>Software Development</h2>
      <ul>
        <li><a href="#">What to learn in a company</a></li>
        <li><a href="#">NET,Php,ANDROID,Networking</a></li>
        <li><a href="#">How to develop a software</a></li>
       
      </ul>
    </div>
    <div class="footbox">
      <h2>Software Training</h2>
      <ul>
        <li><a href="#">Training at a company</a></li>
        <li><a href="#">Learn as you do</a></li>
        <li><a href="#">Career Evaluation</a></li>
        <li><a href="#">Know Your taste</a></li>
        <li class="last"><a href="#">Councelling</a></li>
      </ul>
    </div>
    <br class="clear" />
  </div>
</div>
<!-- ####################################################################################################### -->
<div class="wrapper col6">
  <div id="copyright">
    <p class="fl_left">Copyright &copy; 2011 - All Rights Reserved -www.infoark.in</p>
    <p class="fl_right">infoark solutions</p><!-- hitwebcounter Code START -->
<a href="#" target="_blank">
<img src="http://hitwebcounter.com/counter/counter.php?page=5324470&style=0001&nbdigits=5&type=page&initCount=0" title="Compare OoMA vs Vonage Voice Quality" Alt="Compare OoMA vs Vonage Voice Quality"   border="0" >
</a><br/>
<!-- hitwebcounter.com --><a href="#" title="Site Counter" 
target="_blank" style="font-family: Geneva, Arial, Helvetica, sans-serif; 
font-size: 9px; color: #908C86; text-decoration: underline ;"><strong>Site Counter</strong>
</a>   
  
    <br class="clear" />
  </div>
</div>
</body>
</html>
