<?php
include_once('inc/dbConnect.inc.php');
$query="select * from tab_cat order by cat_name asc";
$res=mysql_query($query);
$count=mysql_num_rows($res);

if($count >0){
   $queryTab="select * from tab_cat order by cat_name asc limit 1";
   $resTab=mysql_query($queryTab);
   $rowTab=mysql_fetch_array($resTab);
   $catId=$rowTab['id'];
   $catTab='cat'.$rowTab['id'];

}else{
   $catId='';
   $catTab='';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PHP Ajax Dynamic Tab | 91 Web Lessons</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script type="text/javascript" src="js/jquery-1.4.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-tab.js"></script>
<script type="text/javascript">

function changeTab(tabId,id){
      $('.tabLink').removeClass('selected');
      $('#'+tabId).addClass('selected');
      var dataString = 'id='+ id;
      $(".tabdiv").fadeIn(400).html('Loading.... <img src="image/ajax-loading.gif" /> ');
      $.ajax({
      type: "POST",
      url: "processed.php",
      data: dataString,
      cache: false,
      success: function(result){
               $(".tabdiv").html(result);
      }
      });
}

changeTab('<?php echo $catTab;?>','<?php echo $catId;?>');
</script>
</head>
<body>
<div id="container">
    <!--top section start-->
    <div id='top'>
         <a href="http://www.91weblessons.com" title="91 Web Lessons" target="blank">
             <img src="image/logo.png" alt="91 Web Lessons" title="91 Web Lessons" border="0"/>
         </a>
    </div>

    <div id='tutorialHead'>
         <div class="tutorialTitle"><h1>PHP Ajax Dynamic Tab</h1></div>
         <div class="tutorialLink"><a href="http://www.91weblessons.com/php-ajax-dynamic-tab" title="PHP Ajax Dynamic Tab"><h1>Tutorial Link</h1></a></div>

    </div>

    <div id="wrapper">
         <?php
         if($count>0){
         ?>
         <div id="tabvanilla" class="widget">
            <ul class="tabnav">
             <?php
                while($row=mysql_fetch_array($res)){
                      echo '<li><a href="javascript:void(0)" class="tabLink" id="cat'.$row['id'].'" onclick=changeTab("cat'.$row['id'].'","'.$row['id'].'")>'.$row['cat_name'].'</a></li>';
        }
        ?>
             </ul>
             <div class="tabdiv"></div>
        </div>
        <?php
         }
         else{
              echo 'No Data Found';
         }
         ?>
    </div>

    <!--fotter section start-->
    <div id="fotter">
         <p>Copyright &copy; 2012 
              <a href="http://www.91weblessons.com" title="91 Web Lessons" target="blank">91 Web Lessons</a>. 
              All rights reserved.
         </p>
    </div>
</div>
</body>
</html>
